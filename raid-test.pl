#!/usr/bin/env perl
use warnings;
use strict;

use Array;

use Getopt::Std;

sub HELP_MESSAGE() {
	print "Usage: raid-test [-hsd]
		-h Print this message
		-s Enable sudo
		-d Enable debug (to STDERR)
	";
}

my %opts=();
getopts('hsd', \%opts);

my $array = new Array();
$array->setSudo(1) if defined $opts{s};
$array->setDebug(1) if defined $opts{d};
$array->detect();
$array->diagnosis();
if($array->getState() != 0) {
	print("RAID is faulted!\n");
} else {
	print("RAID is OK.\n");
}
