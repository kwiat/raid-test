#!/usr/bin/env perl 
use warnings;
use strict;

package Array;

delete @ENV{qw(IFS CDPATH ENV BASH_ENV)};
$ENV{PATH}="/bin:/sbin:/usr/bin:/usr/sbin:/opt/MegaRAID/MegaCli:/opt/HPQacucli/sbin/:/usr/StorMan";
my @raidTools = ("hpacucli", "MegaCli", "MegaCli64", "mdadm", "zpool", "ipssend", "cfggen", "arcconf");
my %raidTools = (); 

sub new {
  my $class = shift;
  my $self = {
    _state => 0,
    _debug => 0,
    _sudo => 0,
  };
  bless $self, $class;
  return $self;
}

sub debug() {
  my ($self, $msg) = @_;
  if ($self->{_debug} == 1) {
    print STDERR "$msg\n";
  }
}

sub _which {
  my ($self, $prog) = @_;
  my $path = `which $prog 2>/dev/null`;
  if($path =~ /no $prog in.*/) { 
    return 0;
  } else {
    chomp($path);
    return $path;
  }
}
sub getState {
  my ($self) = @_;
  return $self->{_state};
}

sub setState {
  my ($self, $state) = @_;
  $self->{_state} = $state if defined($state);
  return 1;

}
sub setSudo {
  my ($self, $sudo) = @_;
  $self->{_sudo} = $sudo if defined($sudo);
  return 1;
}
sub setDebug {
  my ($self, $debug) = @_;
  $self->{_debug} = $debug if defined($debug);
  return 1;
}

  
sub detect {
  my ($self) = @_;
  foreach my $tool (@raidTools) {
    my $path = $self->_which($tool);
    if($path) {
      if($self->{_sudo}) { $path = "sudo $path"; }
      $raidTools{$tool} = "$path";
      $self->debug("Found $path");
    }
  }
  if(keys(%raidTools) == 0) {
    print "No RAID utility found.\n";
    return 0;
  }
}

sub _checkRaid($) {
  my ($self, $util) = @_;
  if($util =~ /hpacucli/) {
    $self->_testHpacucli($raidTools{$util});
  }
  if($util =~ /mdadm/) {
    $self->_testMdadm($raidTools{$util});
  }
  if($util =~ /MegaCli/) {
    $self->_testMegaCli($raidTools{$util});
  }
  if($util =~ /MegaCli64/) {
    $self->_testMegaCli($raidTools{$util});
  }
  if($util =~ /zpool/) {
      $self->_testZpool($raidTools{$util});
  }
  if($util =~ /ipssend/) {
    $self->_testIpssend($raidTools{$util});
  }
  if($util =~ /cfggen/) {
    $self->_testCfggen($raidTools{$util});
  }
  if($util =~ /arcconf/) {
    $self->_testArcconf($raidTools{$util});
  }
}

sub _testHpacucli {
  my ($self, $path) = @_;
  my @ctrlarr = `"$path" ctrl all show`;

  foreach my $line (@ctrlarr) {
    chomp($line);
    if($line =~ /.*Slot (\d){1,2}/) {
      my @devarr = `$path ctrl slot=${1} physicaldrive all show`;
      foreach my $dev (@devarr) {
        chomp($dev);
        $self->debug("Checking $dev");
        if($dev =~ /physicaldrive/ && $dev !~ /OK/) {
          $self->setState(1); 
        }
      }
    }
  }
}

sub _testIpssend {
  my ($self, $path) = @_;
  my $i=1;
  my @status; 
  while(@status = `"$path" getconfig $i`) {
    foreach my $line (@status) {
      chomp($line);
      if($line =~ /Controller number is not valid./) { 
        return 0;
      }
      elsif($line =~ /.*State[\s]+:[\s]+([\w]*).*$/) {
        $self->debug("Checking: $line");
        if(lc($1) !~ /(online|standby)/) {
          $self->setState(1);
        }
      }
    }
    $i++;
  }
}

sub _testArccfg {
  my ($self, $path) = @_;
  my $i=1;
  my @status; 
  while(@status = `"$path" getconfig $i`) {
    foreach my $line (@status) {
      chomp($line);
      if($line =~ /Invalid controller number./) { 
        return 0;
      }
      elsif($line =~ /.*State[\s]+:[\s]+([\w]*).*$/) {
        $self->debug("Checking: $line");
        if(lc($1) !~ /(online|standby)/) {
          $self->setState(1);
        }
      }
    }
    $i++;
  }
}

sub _testMdadm {
  my ($self, $path) = @_;
  my $mdstate = system("$path -Dts > /dev/null");
    $self->debug("Checking: $mdstate");
  if($mdstate != 0) {
    $self->setState(1);
  }
}

sub _testMegaCli {
  my ($self, $path) = @_;
  my @ldinfo = `$path -LDInfo -LALL -aALL`;

  foreach my $line (@ldinfo) {
    chomp($line);
    if($line =~ /^State.*\: ([a-zA-Z]+)$/) {
      $self->debug("Checking: $line");
      if($1 !~ /Optimal/) {
        $self->setState(1);
      } 
    }
  }
}
sub _testCfggen {
  my ($self, $path) = @_;
  my @ctrlrs = `$path list | grep -E -A 10 -- "([-]+)" | awk '/(\\w)/ {print \$1}'`;
  foreach my $ctrl (@ctrlrs) {
    chomp($ctrl);
    $self->debug("Checking: $ctrl");
    my $status = `$path $ctrl status | awk '/Volume state/ {print \$4}'`;
    if($status !~ /Optimal/) {
      $self->setState(1);
    }
  }
}



sub _testZpool {
  my ($self, $path) = $_;
  my $zpoolStatus = `zpool status -x`;
  if($zpoolStatus !~ /all pools are healthy/) {
    $self->debug("Checking: $zpoolStatus");
    $self->setState(1);
  }
}

sub diagnosis {
  my ($self) = @_;
  foreach my $tool (keys(%raidTools)) {
    $self->_checkRaid($tool);
  }
}

1;
